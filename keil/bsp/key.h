/**
  **************************************************************************
  * @file:    key.h
  * @author:
  * @version:
  * @date:  6,25,2019
  * @brief:
   *************************************************************************
  * @attention:
  **************************************************************************
  */

#ifndef __KEY_H
#define __KEY_H


#define    MCU_STM32      1    



#ifdef  MCU_STM32
#include "stm32f10x.h"
#else
#include "bsp.h"
#include "stm8s.h"
#endif

#include <stdbool.h>

//==============================================================================
// 按键定义
//==============================================================================
#define  n_20ms                (  20u)
#define  n_100ms               ( 100u)
#define  n_250ms               ( 250u)
#define  n_500ms               ( 500u)
#define  n_1000ms              (1000u)
#define  n_1500ms              (1500u)

#define  n_3000ms              (3000u)

#define  n_Null                (   0)
#define  n_brr_down            (1<<3)      	//(1<<2)
#define  n_brr_up              (1<<0)      	//(1<<3)
#define  n_down                (1<<13)     	//(1<<4)
#define  n_set                 (1<<12)      //(1<<4)
#define  n_up                  (1<<0)      	//(1<<7)
#define  n_power               (1<<14)
//#define  n_PowerModeSet        (n_Power | n_brr_up)

#define  n_KEYUSEBIT           (0x2e)
#define  n_KEYCOUNT            (   2)

#define  KEY_HOLD_COUNT_MAX    (6000)

//==============================================================================
// key status
//==============================================================================
typedef enum {
	KEY_ACTION_IDLE=0,
	KEY_ACTION_FIRST,
	KEY_ACTION_CHANGE,
	KEY_ACTION_HOLD,
	KEY_ACTION_OFF,
} KeyActionStateEnum;

//==============================================================================
// 按键确认按下后，根据按键按下的时间来判断按键的产生的功能状态
//==============================================================================
typedef enum {
	KEY_HOLD_NORMAL = 0,
	KEY_HOLD_LONG,
	KEY_HOLD_WAIT,
	KEY_HOLD_LOOP,
} KeyHoldStateEnum;
//==============================================================================
// key value
//==============================================================================
typedef enum {
	KEY_ID_NULL = 0,

	KEY_ID_POWER,
	KEY_ID_POWER_LONG,
	KEY_ID_POWER_LOOP,

	KEY_ID_SET,
	KEY_ID_SET_LONG,
	KEY_ID_SET_LOOP,

	KEY_ID_UP,
	KEY_ID_UP_LONG,
	KEY_ID_UP_LOOP,

	KEY_ID_DOWN,
	KEY_ID_DOWN_LONG,
	KEY_ID_DOWN_LOOP,

} KeyIDEnum;

//==============================================================================
// key
//==============================================================================
typedef struct {
	bool              key_flg_en:1;
	KeyIDEnum         key_id;
	uint16_t          key_value;
	uint16_t          val_old;
	uint16_t          val_new;
	uint16_t          count;
	uint16_t          mask;
	uint16_t          start_up_cnt;

} keyData_t;

extern  keyData_t    key_data;
extern  bool         key_FlgAction;

//======================================
//函数声明
//======================================
void   key_Init ( void );
void   key_module_clock ( void );

void   keyPoll ( void );
void   key_statr_up_explain ( void );
void   key_decode ( void ); //按键解码

void HalKeyPol(void);

#endif
/****************end**********************/
