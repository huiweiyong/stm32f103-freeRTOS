/**
  **************************************************************************
  * @file:   key.c
  * @author: 翚伟勇
  * @version:
  * @date:  6,25,2019
  * @brief:
   *************************************************************************
  * @attention:
  按键处理有单击、长按、长按循环
  单击：		如果后续没有长按积循环功能，则按下就有效
  长按： 		如果后续没有循环功能，长按时间到发出信号，如有循环则进入循环
  **************************************************************************
  */
#include "key.h"

//=========================================================

static   bool            key_flg_timer_normal;    // 按键使用的基准时钟
static   bool            key_flg_timer_start_up;  // 按键使用的基准时钟
static   uint16_t        key_hold_count;
static   bool            key_ReleaseActive;
static   uint8_t         keyID;

static   uint16_t        t_single;     // 单击计时
static   uint16_t        t_long;       // 长按计时
static   uint16_t        t_loop;       // 循环计时

keyData_t                key_data;
KeyHoldStateEnum         key_hold_state;
bool                     key_FlgAction;

//==============================================================================
//  ROM TABLE
//==============================================================================
volatile const struct {
	uint16_t       KeyData;          // 检测到的按键键值
	bool           long_en;          //0：没有长按；1：有长按
	bool           loop_en;          //0：无循环；1：有循环
	uint16_t       single_time;      //单击识别的时间
	uint16_t       long_time;        // 长按，识别的时间
	uint16_t       loop_time;        //

	KeyIDEnum      id_single;        // 对应的 single_time 的a
	KeyIDEnum      id_long;          // 对应的 single_time 的b
	KeyIDEnum      id_loop;          // 对应的 long_time

}
DT_KEYkeep[] = {
	{n_Null,  0, 0,       0,        0, 0,         KEY_ID_NULL,              KEY_ID_NULL,   KEY_ID_NULL      },

	{n_power, 1, 0, n_100ms, n_1000ms, 0,        KEY_ID_POWER,	      KEY_ID_POWER_LONG,   KEY_ID_POWER_LOOP},
	{n_set,   0, 0, n_100ms, n_1500ms, 0,          KEY_ID_SET,          KEY_ID_SET_LONG,   KEY_ID_SET_LOOP  },
	{n_up,    1, 1, n_100ms, n_1000ms, n_20ms,      KEY_ID_UP,           KEY_ID_UP_LONG,   KEY_ID_UP_LOOP   },
	{n_down,  1, 1, n_100ms, n_1000ms, n_20ms,    KEY_ID_DOWN,         KEY_ID_DOWN_LONG,   KEY_ID_DOWN_LOOP },

};

/***************************************************************************
 * @ fn
 * @ brief
 * @ param
 * @ param
 * @ retval
 **/
void key_Init ( void )
{
	key_data.key_id     = KEY_ID_NULL;
	key_data.val_old    = n_Null;
	key_FlgAction       = false;
}

/***************************************************************************
 * @ fn
 * @ brief
 * @ param
 * @ param
 * @ retval
 **/
void key_module_clock ( void )
{
	key_flg_timer_normal = true;
}

/***************************************************************************
 * @ fn
 * @ brief   扫描按键端口
 * @ param
 * @ param
 * @ retval
 **/
uint16_t key_PortScan ( void )
{
//	uint8_t key_val;
#ifdef   MCU_STM32
	uint16_t key_val;
	key_val = ( GPIO_ReadInputData ( GPIOB ) &0x7001 );
//	key_val = key_val| ( ( GPIO_ReadInputData ( GPIOE ) &0x40 ) >>3 );
	key_val = ~key_val;
	key_val = key_val&0x7001;
#else
	key_val = ( GPIO_ReadInputData ( GPIOB ) &0x23 );
	key_val = key_val| ( ( GPIO_ReadInputData ( GPIOE ) &0x40 ) >>3 );

	key_val = ~key_val;
	key_val = key_val&0x2B;
#endif

	return ( key_val );
}

/***************************************************************************
 * @ fn
 * @ brief
 * @ param
 * @ param
 * @ retval
 **/
void keyPoll ( void )
{
	key_data.key_value = key_PortScan();
	key_decode();
}

/***************************************************************************
 * @ fn
 * @ brief
 * @ param
 * @ param
 * @ retval
 **/
uint8_t key_index_get ( uint16_t bData )
{
	uint8_t i;

	for ( i = 1; i < ( sizeof ( DT_KEYkeep ) / sizeof ( DT_KEYkeep[0] ) ) ; i++ ) {
		if ( bData == DT_KEYkeep[i].KeyData ) {
			return i;
		}
	}
	return ( 0 );

}

/***************************************************************************
 * @ fn
 * @ brief
 * @ param
 * @ param
 * @ retval
 **/
void key_decode ( void )
{
	uint8_t               key_index;
	KeyActionStateEnum    status;

	key_data.count   = 0;
	key_data.val_new = key_data.key_value;
	if ( ( key_data.val_new == n_Null )
	     && ( key_data.val_old == n_Null ) ) {

		status =  KEY_ACTION_IDLE;	 // 松开按键
	}
	else if ( ( key_data.val_new == key_data.val_old )
	          && ( key_data.val_new !=n_Null ) ) {

		status =  KEY_ACTION_HOLD;
	}
	else if ( ( key_data.val_new == n_Null )
	          && ( key_data.val_old != n_Null ) ) {

		status =  KEY_ACTION_OFF;    // 松开按键
	}
	else if ( ( key_data.val_old == n_Null )
	          && ( key_data.val_new != n_Null ) ) {

		status =  KEY_ACTION_FIRST;    // 按下按键
	}
	else if ( ( key_data.val_new !=n_Null )
	          && ( key_data.val_old!=n_Null )
	          && ( key_data.val_old!=key_data.val_new ) ) {

		status =  KEY_ACTION_CHANGE;    // 改变按键
	}
	else {
	}

	switch ( status ) {
		case KEY_ACTION_IDLE:
			key_ReleaseActive = false;
			status =KEY_HOLD_NORMAL;
			keyID =KEY_ID_NULL;
			break;
		case KEY_ACTION_FIRST:
			key_index   = key_index_get ( key_data.val_new );
			t_single    = DT_KEYkeep[key_index].single_time;
			t_long      = DT_KEYkeep[key_index].long_time;
			break;
		case KEY_ACTION_CHANGE:
			/******************************
			当有组合键时，就会运行到这个状态，因为组合键不可能同时按下的。
			******************************/
			key_index = key_index_get ( key_data.val_new );
			t_single  = DT_KEYkeep[key_index].single_time;
			t_long    = DT_KEYkeep[key_index].long_time;
			break;
		case KEY_ACTION_HOLD:
			if ( key_hold_count < KEY_HOLD_COUNT_MAX ) {
				key_hold_count++;
			}
			switch ( key_hold_state ) {
				case KEY_HOLD_NORMAL:  //短按设别
					if ( key_hold_count > t_single ) {
						key_hold_count = 0;
						key_index = key_index_get ( key_data.val_new );

						if ( DT_KEYkeep[key_index].long_en ) { /* 这里判断是否是按下时马上有效还是弹起才有效 */
							//按键弹起才有效
							key_ReleaseActive =true;
							keyID = DT_KEYkeep[key_index].id_single;
							t_long = DT_KEYkeep[key_index].long_time;
							key_hold_count = 0;
							key_hold_state = KEY_HOLD_LONG;
						}
						else {
							//按键只有单击，没有长按以及周期
							key_data.key_id = DT_KEYkeep[key_index].id_single;
							key_data.key_flg_en = true;
							key_hold_state = KEY_HOLD_WAIT;
						}
					}
					break;
				case KEY_HOLD_LONG:
					if ( key_hold_count > t_long ) {
						key_hold_count = 0;
						key_ReleaseActive = false ;		//取消按键弹起有效
						key_index = key_index_get ( key_data.val_new );
						t_loop    = DT_KEYkeep[key_index].loop_time;

						if ( DT_KEYkeep[key_index].loop_en ) {
							//进入循环周期
							key_data.key_id = DT_KEYkeep[key_index].id_long;
							key_data.key_flg_en = true;
							key_hold_state = KEY_HOLD_LOOP;
						}
						else {
							/*没有循环，长按时间到则动作*/
							key_hold_count = 0;
							key_data.key_id = DT_KEYkeep[key_index].id_long;
							key_data.key_flg_en = true;
							key_hold_state = KEY_HOLD_WAIT;
						}
					}
					break;
				case KEY_HOLD_WAIT:
					key_hold_count = 0;
					key_data.key_id=KEY_ID_NULL;
					break;
				case KEY_HOLD_LOOP:    //进入长按，周期发送
					if ( key_hold_count > t_loop ) {
						key_hold_count = 0;
						key_index = key_index_get ( key_data.val_new );
						key_data.key_id = DT_KEYkeep[key_index].id_loop;
						key_data.key_flg_en = true;
					}
					break;
				default:
					break;
			}
			break;
		case KEY_ACTION_OFF:
			key_hold_count  = 0;
			key_hold_state = KEY_HOLD_NORMAL;

			//按键有长按功能的，短按在弹起后才识别
			if ( key_ReleaseActive==true ) {
				key_ReleaseActive = false;
				key_data.key_flg_en = true;
				key_data.key_id =keyID;
			}
			break;
		default:
			break;
	}
	/* 给原按键值更新*/
	key_data.val_old = key_data.val_new;
}

void HalKeyPol ( void )
{
}



/***************************************/




